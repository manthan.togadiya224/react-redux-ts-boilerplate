import { History } from "history";

export interface IRouteProps {
    history: History;
}

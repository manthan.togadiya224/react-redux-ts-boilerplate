import { RouterState } from "react-router-redux";

export interface IRootState {
  router?: RouterState;
  appState: IAppState;
}
export interface IAppState {
  key: boolean;
}

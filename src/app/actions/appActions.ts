import { createAction } from "redux-actions";
export namespace AppActions {
  export enum Type {
    SAMPLE = "SAMPLE",
  }
  export const changeTab = createAction<string>(Type.SAMPLE);
}

export type AppActions = Omit<typeof AppActions, "Type">;

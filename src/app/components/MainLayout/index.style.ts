import { css } from "emotion";

export const pageContainer = css``;

export const innerContainer = css`
  width: 100%;
`;

export const mainLayout = css`
  max-width: 1500px;
  margin: 0 auto;
  padding: 40px 60px 40px 60px;
  height: calc(100vh);
  overflow: hidden;
  background-color: #f8f8f8;
`;

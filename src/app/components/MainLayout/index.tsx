import * as React from "react";
import { Layout, Menu, Icon } from "antd";

// import { NavBar } from "app/containers/NavBar/NavBar";
import * as style from "./index.style";
import { RouteComponentProps } from "react-router";

export namespace MainLayout {
  export interface IProps extends RouteComponentProps<void> {}
}
export class MainLayout extends React.Component<MainLayout.IProps> {
  constructor(p: any, s: any) {
    super(p, s);
  }
  renderTopBar(): JSX.Element {
    // let NavBarAny = NavBar as any;
    return (
      <div className="topBar">
        {/* <span className="clinicText">Title</span> */}
        {/* <NavBarAny history={this.props.history} /> */}
      </div>
    );
  }
  render(): JSX.Element {
    return (
      <Layout className={style.mainLayout}>
        <div className={style.innerContainer}>
          {this.renderTopBar()}
          <div className={style.pageContainer}>{this.props.children}</div>
        </div>
      </Layout>
    );
  }
}

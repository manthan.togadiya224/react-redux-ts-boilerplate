import { combineReducers } from "redux";
import { appReducer } from "app/reducers/app";
import { routerReducer, RouterState } from "react-router-redux";
import { IRootState } from "../models";

// NOTE: current type definition of Reducer in 'react-router-redux' and 'redux-actions' module
// doesn't go well with redux@4
export const rootReducer = combineReducers<IRootState>({
  appState: appReducer as any,
  router: routerReducer as any
});

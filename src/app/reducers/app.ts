import { handleActions } from "redux-actions";
import { initialState } from "../states";
import { IRootState } from "../models";
import { AppActions } from "app/actions";
import * as _ from "lodash";

export const appReducer = handleActions<IRootState["appState"]>(
  {
    [AppActions.Type.SAMPLE]: (state, action) => {
      if (action.payload) {
        return _.assign({ ...state }, { currTab: action.payload });
      } else {
        return state;
      }
    }
  },
  initialState.appState
);

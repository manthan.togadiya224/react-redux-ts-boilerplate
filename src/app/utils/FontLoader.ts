const WebFont = require("webfontloader");
const path = require("path");
const FONT_PATH = "/assets";
export function loadFonts(): void {
    let fontPath = path.join(FONT_PATH,"AvenirNext-Medium.ttf");
    let fontFormat = path.extname(fontPath);
    let fontFamily = path.basename(fontPath, fontFormat);
    let dir = path.dirname(fontPath) + "/";
    WebFont.load({
        custom: {
            families: [fontFamily]
        }
    });
    let newStyle = document.createElement("style");
    newStyle.appendChild(document.createTextNode(
        `@font-face {\
            font-family: ${fontFamily};\
            src:local('${fontFamily}'), url('${dir}${fontFamily}${fontFormat}')}`));
    document.head.appendChild(newStyle);
}

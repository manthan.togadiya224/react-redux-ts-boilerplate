export default class LocalStorage {
  static saveNonce(nonce: string): void {
    if (typeof(Storage) !== "undefined") {
      localStorage.setItem("nonce", nonce);
    }
  }

  static loadNonce(): string {
    if (typeof(Storage) !== "undefined") {
      const nonce: string = localStorage.getItem("nonce");
      localStorage.removeItem("nonce");
      return nonce;
    }
    return "";
  }

  static saveAccessToken(accessToken: string): void {
    if (typeof(Storage) !== "undefined") {
      localStorage.setItem("access_token", accessToken);
    }
  }

  static loadAccessToken(): string {
    if (typeof(Storage) !== "undefined") {
      const accessToken: string = localStorage.getItem("access_token");
      return accessToken;
    }
    return "";
  }

  static deleteAccessToken(): void {
    if (typeof(Storage) !== "undefined") {
      localStorage.removeItem("access_token");
    }
  }
}

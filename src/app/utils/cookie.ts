export default class Cookie {
  static set(name, value, days, path = "/"): void {
    let expires;
    let date;
    if (days) {
      date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = "; expires=" + date.toGMTString();
    } else {
      expires = "";
    }

    const dir = path || "/";
    document.cookie = name + "=" + value + expires + "; path=" + dir;
  }

  static gette(name): string {
    const nameEQ = name + "=";
    const ca = document.cookie.split(";");
    for (let i = 0; i < ca.length; i += 1) {
      let c = ca[i];
      while (c.charAt(0) === " ") {
        c = c.substring(1, c.length);
      }
      if (c.indexOf(nameEQ) === 0) {
        return c.substring(nameEQ.length, c.length);
      }
    }
    return null;
  }

  static get(name): string {
    const x = this.gette(name);
    return x && (x === "null" ? undefined : decodeURIComponent(x));
  }

  static delete(name): void {
    this.set(name, "", -1);
  }

  static deleteAll(): void {
    this.delete("role");
    this.delete("userId");
    this.delete("name");
    this.delete("exp");
  }
}

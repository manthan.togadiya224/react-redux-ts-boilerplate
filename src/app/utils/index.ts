import * as _ from 'lodash';

export function omit<T extends object, K extends keyof T>(target: T, ...omitKeys: K[]): Omit<T, K> {
  return (Object.keys(target) as K[]).reduce(
    (res, key) => {
      if (!omitKeys.includes(key)) {
        (res as any)[key] = target[key];
      }
      return res;
    },
    {} as Omit<T, K>
  );
}

export function getDiffObj(oldValue, keysToCheck, newValue, primaryKey): any {
  const pickedOldValue = _.pick(oldValue, keysToCheck);
  const pickedNewValue = _.pick(newValue, keysToCheck);

  let diffObj = {};
  if (_.isEqual(_.keys(pickedOldValue), _.keys(pickedNewValue))) {
    const keys = _.keys(pickedOldValue);
    keys.forEach((key) => {
      if (newValue[key] !== oldValue[key]) {
        diffObj = diffObj || {};
        diffObj[key] = newValue[key];
      }
    });
  } else {
    return;
  }

  diffObj[primaryKey] = oldValue[primaryKey];
  return diffObj;
}

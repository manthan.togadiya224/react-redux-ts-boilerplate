import { hot } from "react-hot-loader";
import "./theme/themeOverride.less";
import router from "app/router";
import { loadFonts } from "./utils/FontLoader";

export const App = hot(module)(() => router());
loadFonts();

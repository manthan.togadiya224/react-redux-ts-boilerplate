import * as React from "react";
import { Route, Switch } from "react-router";
import { Router, Redirect } from "react-router-dom";
import createHistory from "history/createBrowserHistory";
import { MainLayout } from "app/components/MainLayout";
import { HomePage } from "./highLevelComponents/homePage";
export const history = createHistory({});
// export const DETAIL_PATH = `/${Tabs.MAIN}/:Id`;

// login
export const ROOT_PATH = `/home`;
export const LOGIN_PATH = `/login`;
const routes = () => {
  let MainLayoutAny = MainLayout as any;
  return (
    <Router history={history}>
      {/* <LoginPageAny history={history}> */}
      <div style={{ height: "100%", width: "100%", backgroundColor: "#f8f8f8" }}>
        <Switch>
          {/* <Route exact path={OUT_OF_MAIN_LAYOUT} component={OutOfMainLayout} /> */}
          <MainLayoutAny history={history}>
            <Route
              exact
              path="/"
              render={() => {
                // window.location.href = ROOT_PATH; // push state not working properly
                return <Redirect to={ROOT_PATH} />;
              }}
            />
            <Switch>
              <Route exact path={ROOT_PATH} component={() => <HomePage history={history} />} />
            </Switch>
          </MainLayoutAny>
        </Switch>
      </div>
      {/* </LoginPageAny> */}
    </Router>
  );
};
export default routes;

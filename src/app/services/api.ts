// a library to wrap and simplify api calls
import apisauce from "apisauce";
import LocalStorage from "app/utils/localStorage";

const create = (baseURL = process.env.SERVER_BASE_URL) => {
  //
  // Create and configure an apisauce-based api object.
  //
  const api = apisauce.create({
    // base URL is read from the "constructor"
    baseURL,
    // here are some default headers
    headers: {
      "Authorization": LocalStorage.loadAccessToken()
    },
    // 10 second timeout...
    timeout: 10000
  });
  return api;
};

export default {
  create
};

import API from "./api";
import AccessToken from "./AccessToken";
import { ApiResponse } from "apisauce";
import LocalStorage from "app/utils/localStorage";

export interface IResponseFormat {
  error: boolean;
  errorMessage: string;
  results: any[];
}

export const utils = {
  getResponseResults: (response: ApiResponse<{}>): IResponseFormat | any => {
    if (response.ok && !(response.data as IResponseFormat).error) {
      return (response.data as IResponseFormat).results;
    } else {
      if (!response.data) {
        response["error"] = true; // tslint:disable-line
        return response;
      }
      response.data = response.data;
      response.data["error"] = true; // tslint:disable-line
      return response.data;
    }
  }
};

const DebugConfig = {
  useFixtures: false
};

// add mock data here
const FixtureAPI = "";

let api = API.create();

export function initialiseApi(): void {
  api = API.create();
  const refreshTokenInterval = 20 * 60 * 1000;
  setInterval(async () => {
    const response = await AccessToken.fetchNewAccessToken();
    if (!response.error) {
      LocalStorage.saveAccessToken(response[0].accessToken);
    }
  }, refreshTokenInterval);
}

export async function fetchNewAccessToken(): Promise<string> {
  const response = await AccessToken.fetchNewAccessToken();
  if (!response.error) {
    LocalStorage.saveAccessToken(response[0].accessToken);
  }
  return response[0].accessToken;
}

export default {
  AccessToken
};

export { api };

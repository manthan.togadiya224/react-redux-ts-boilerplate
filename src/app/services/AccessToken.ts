import { api, IResponseFormat, utils } from "./";
import LocalStorage from "app/utils/localStorage";

const fetchNewAccessToken = async (): Promise<any> => {
    const response = await api.post("authentication/fetchNewAccessToken", {
        accessToken: LocalStorage.loadAccessToken()
    });
    const results = utils.getResponseResults(response);
    if (!results.error) {
        return results;
    } else {
        throw results;
    }
};

export default {
    fetchNewAccessToken
};

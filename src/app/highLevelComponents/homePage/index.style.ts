import { css } from "emotion";

export const center = css`
display: flex;
flex-direction: column;
width: 150px;
margin: auto;
height: calc(100vh - 200px);
justify-content: center;
`;

export const btn = css`
margin: 10px 0px;
`;

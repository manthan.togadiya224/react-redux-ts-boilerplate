import * as React from "react";
import { connect } from "react-redux";
import { IRouteProps } from "../../models/RouteProps";
import { IRootState } from "../../models";
import { Button } from "antd";
import * as style from "./index.style";

export namespace HomePage {
  export interface IProps extends IRouteProps {}
}

@connect(
  (state: IRootState): Pick<HomePage.IProps, any> => {
    return {};
  }
)
export class HomePage extends React.Component<HomePage.IProps> {
  componentDidMount(): void {}
  render(): JSX.Element {
    return (
      <div className={style.center}>
        <Button className={style.btn}>Log In</Button>
        <Button className={style.btn}>Sign Up</Button>
      </div>
    );
  }
}

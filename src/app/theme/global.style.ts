import styled, { css } from "react-emotion";

export const cardHeader = css`
    font-weight: bolder;
    margin: 20px 20px 20px 10px;
    font-size: 20px;
    color: #404040;
`;

export const cardItemText = css`
    font-weight: 520;
    color: #777777;
`;

export const cardBody = css`
    background: white;
    padding: 10px 10px 10px 10px;
    border-radius: 15px;
`;

export const loadingContainer = css`
    width: 100%;
    text-align: center;
    padding-top: 10px;
`;
export const lightColorText = css`
    color: #b7b7b7;
    font-weight: 500;
`;
